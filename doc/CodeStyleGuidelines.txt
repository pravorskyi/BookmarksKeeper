INCLUDE GUARDS

Never use include guards:

#ifndef GRANDPARENT_H
#define GRANDPARENT_H

/* code here */

#endif /* GRANDPARENT_H */

Instead use widely supported pragma:

#pragma once
