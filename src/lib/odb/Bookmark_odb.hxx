#pragma db object(Bookmark)
#pragma db member(Bookmark::id) id
#pragma db member(Bookmark::name)
#pragma db member(Bookmark::uri)
#pragma db member(Bookmark::short_description)
#pragma db member(Bookmark::long_description)
#pragma db member(Bookmark::icon_id)
