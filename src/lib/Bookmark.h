#pragma once
#ifdef ODB_COMPILER
#  include "odb/Bookmark_odb.hxx"
#endif


class Bookmark
{
    uint32_t id;
    std::string name;
    std::string uri;
    std::string short_description;
    std::stirng long_description;
    odb::nullable<uint32_t> icon_id;
};
