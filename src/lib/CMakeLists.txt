project(bookmarkskeeper-lib CXX)

find_package(Qt5 ${QT_MIN_VERSION} CONFIG REQUIRED COMPONENTS Core)

set(CMAKE_AUTOMOC ON)


file(GLOB SOURCES
   *.cpp
)

add_library(${PROJECT_NAME} SHARED ${SOURCES})
target_link_libraries (${PROJECT_NAME} ${Qt5Core_LIBRARIES})

#install
install(TARGETS ${PROJECT_NAME} LIBRARY DESTINATION lib)
