#include "CompactWindow.h"
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTreeView>


CompactWindow::CompactWindow(QWidget * parent) :
    QWidget(parent)
{
    QLineEdit * edt = new QLineEdit;
    edt->setPlaceholderText("Search bookmarks…");
    edt->setEnabled(false);

    QTreeView * view = new QTreeView;
    view->setEnabled(false);

    QStatusBar * statusBar = new QStatusBar;
    statusBar->showMessage(tr("Loading:"));
    QProgressBar * progressBar = new QProgressBar(statusBar);
    progressBar->setMinimum(0);
    progressBar->setMaximum(0);
    progressBar->setAlignment(Qt::AlignRight);
    statusBar->addPermanentWidget(progressBar);

    QVBoxLayout * lt = new QVBoxLayout(this);
    lt->addWidget(edt);
    lt->addWidget(view);
    lt->addWidget(statusBar);
}
